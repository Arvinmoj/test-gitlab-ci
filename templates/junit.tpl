<?xml version="1.0" ?>
<testsuites name="trivy">
{{- range . -}}
{{- $target := .Target }}
{{- $type := .Type }}
{{- $failures := len .Vulnerabilities }}
   <testsuite tests="{{ $failures }}" failures="{{ $failures }}" name="{{ $target }}" errors="0" skipped="0" time="">
   {{- if $type }}
       <properties>
           <property name="type" value="{{ $type }}"></property>
       </properties>
   {{- end }}
       {{ range .Vulnerabilities }}
       <testcase classname="{{ .PkgName }}-{{ .InstalledVersion }}" name="[{{ .Vulnerability.Severity }}] {{ .VulnerabilityID }}" time="">
           <failure message="{{ escapeXML .Title }}" type="description">{{ escapeXML .Description }}</failure>
       </testcase>
   {{- end }}
   </testsuite>

{{- if .MisconfSummary }}
   {{- $failures := add .MisconfSummary.Successes .MisconfSummary.Failures }}
   <testsuite tests="{{ $failures }}" failures="{{ .MisconfSummary.Failures }}" name="{{ $target }}" errors="0" skipped="{{ .MisconfSummary.Exceptions }}" time="">
   {{- if $type }}
       <properties>
           <property name="type" value="{{ $type }}"></property>
       </properties>
   {{- end }}
       {{ range .Misconfigurations }}
       <testcase classname="{{ .Type }}" name="[{{ .Severity }}] {{ .ID }}" time="">
       {{- if (eq .Status "FAIL") }}
           <failure message="{{ escapeXML .Title }}" type="description">{{ escapeXML .Description }}</failure>
       {{- end }}
       </testcase>
   {{- end }}
   </testsuite>
{{- else }}
   <testsuite tests="0" failures="0" name="{{ $target }}" errors="0" skipped="0" time="">
   {{- if $type }}
       <properties>
           <property name="type" value="{{ $type }}"></property>
       </properties>
   {{- end }}
   </testsuite>
{{- end }}
{{- end }}
</testsuites>
